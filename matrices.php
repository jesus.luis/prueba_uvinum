<?php

function getMatrixMax(array $matrix) :? int
{
    // Write some code to traverse all the elements in the matrix and return the biggest number!
    $max = null;

    foreach($matrix as $subMatrix) {
        $subMax = max($subMatrix);

        $max = ($subMax > $max || !$max) ? $subMax : $max;
    }

    return $max;
}

function getMatrixMaxFunctional(array $matrix):? int
{
    return max(array_map(function($subMatrix) { return max($subMatrix); },  $matrix));
}

$matrixPositive = [
    [10, 100, 3],
    [12, 200, 154],
    [3, 30, 2],
];

$matrixMixed = [
    [-10, 100, 3],
    [12, -200, 154],
    [3, 30, -2],
];

$matrixNegative = [
    [-10, -100, -3],
    [-12, -200, -154],
    [-3, -30, -2],
];

echo getMatrixMax($matrixPositive).PHP_EOL; // Should return 200
echo getMatrixMax($matrixMixed).PHP_EOL; // Should return 154
echo getMatrixMax($matrixNegative).PHP_EOL; // Should return -2


echo getMatrixMaxFunctional($matrixPositive).PHP_EOL; // Should return 200
echo getMatrixMaxFunctional($matrixMixed).PHP_EOL; // Should return 154
echo getMatrixMaxFunctional($matrixNegative).PHP_EOL; // Should return -2

