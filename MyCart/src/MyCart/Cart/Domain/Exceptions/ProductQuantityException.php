<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;

class ProductQuantityException extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Quantity must be greater than Zero and lower than limit');
    }
}
