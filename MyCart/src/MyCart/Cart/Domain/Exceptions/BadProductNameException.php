<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class BadProductNameException extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Product name length error');
    }

}
