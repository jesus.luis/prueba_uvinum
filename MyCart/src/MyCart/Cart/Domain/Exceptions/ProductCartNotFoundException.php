<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class ProductCartNotFoundException extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Product is not present');
    }
}
