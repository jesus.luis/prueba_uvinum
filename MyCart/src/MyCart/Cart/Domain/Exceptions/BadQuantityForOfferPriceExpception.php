<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class BadQuantityForOfferPriceExpception extends \ErrorException
{

    protected function errorMessage(): string
    {
        return sprintf('Minimum quantity for offer price must be 0 or higher');
    }
}
