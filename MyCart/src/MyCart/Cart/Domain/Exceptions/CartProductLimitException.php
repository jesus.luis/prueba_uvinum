<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class CartProductLimitException extends \ErrorException
{
    private int $limit;

    /**
     * CartProductLimitException constructor.
     * @param int $limit
     */
    public function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf("Product limit exceeded {$this->limit}");
    }
}
