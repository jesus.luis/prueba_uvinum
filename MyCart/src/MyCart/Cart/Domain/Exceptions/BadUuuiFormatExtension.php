<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class BadUuuiFormatExtension extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Bad uuid format');
    }
}
