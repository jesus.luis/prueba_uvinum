<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class OfferPriceLowerThanNormalPriceException extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Offer price must be higher than normal price');
    }
}
