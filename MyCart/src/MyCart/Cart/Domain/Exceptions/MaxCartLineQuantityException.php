<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class MaxCartLineQuantityException extends \ErrorException
{
    private $limit;

    public function __construct($limit)
    {

        $this->limit = $limit;
    }

    protected function errorMessage(): string
    {
        return sprintf("Product limit on cart line exceeded {$this->limit}");
    }
}
