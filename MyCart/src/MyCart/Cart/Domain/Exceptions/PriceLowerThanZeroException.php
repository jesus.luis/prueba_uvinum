<?php


namespace Jesusmlg\MyCart\Cart\Domain\Exceptions;


class PriceLowerThanZeroException extends \ErrorException
{
    protected function errorMessage(): string
    {
        return sprintf('Price must be higher than zero');
    }
}
