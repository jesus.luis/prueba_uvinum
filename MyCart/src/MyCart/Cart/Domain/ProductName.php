<?php


namespace Jesusmlg\MyCart\Cart\Domain;


use Jesusmlg\MyCart\Cart\Domain\Exceptions\BadProductNameException;

class ProductName
{
    /**
     * @var string
     */
    private string $value;

    /**
     * ProductName constructor.
     * @param string $value
     * @throws BadProductNameException
     */
    public function __construct(string $value)
    {
        $this->checkLength($value);

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue() :string
    {
        return $this->value;
    }

    /**
     * @param $value
     * @throws BadProductNameException
     */
    private function checkLength($value): void
    {
        if(strlen($value) < 3 || strlen($value) > 255) {
            throw new BadProductNameException();
        }
    }

}
