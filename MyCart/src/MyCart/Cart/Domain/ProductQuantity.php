<?php


namespace Jesusmlg\MyCart\Cart\Domain;

use Jesusmlg\MyCart\Cart\Domain\Exceptions\MaxCartLineQuantityException;
use Jesusmlg\MyCart\Cart\Domain\Exceptions\ProductQuantityException;

class ProductQuantity
{

    const MAX_QUANTITY = 50;

    /**
     * @var int
     */
    private int $value;

    /**
     * ProductQuantity constructor.
     * @param int $value
     * @throws MaxCartLineQuantityException
     * @throws ProductQuantityException
     */
    public function __construct(int $value)
    {
        $this->mustLowerThanMaxQuantity($value);
        $this->mustBeGreaterThanZero($value);

        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue() :int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @throws MaxCartLineQuantityException
     */
    private function mustLowerThanMaxQuantity (int $value) :void
    {
        if($value > self::MAX_QUANTITY) {
            throw new MaxCartLineQuantityException(self::MAX_QUANTITY);
        }
    }

    /**
     * @return int
     */
    public function getMaxQuantity() :int
    {
        return self::MAX_QUANTITY;
    }

    /**
     * @param int $value
     * @throws ProductQuantityException
     */
    public function mustBeGreaterThanZero(int $value): void
    {
        if($value < 1) {
            throw new ProductQuantityException();

        }
    }

}
