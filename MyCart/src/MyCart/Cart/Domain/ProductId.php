<?php


namespace Jesusmlg\MyCart\Cart\Domain;


use Jesusmlg\MyCart\Cart\Domain\Exceptions\BadUuuiFormatExtension;

class ProductId
{
    private string $value;

    /**
     * ProductId constructor.
     * @param string $uuid
     * @throws BadUuuiFormatExtension
     */
    public function __construct(string $uuid)
    {
        if (!preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $uuid)) {
            throw new BadUuuiFormatExtension();
        }

        $this->value = $uuid;
    }

    /**
     * @return string
     */
    public function getValue() :string
    {
        return $this->value;
    }
}
