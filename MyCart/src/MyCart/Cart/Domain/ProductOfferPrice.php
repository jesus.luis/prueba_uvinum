<?php


namespace Jesusmlg\MyCart\Cart\Domain;

use Jesusmlg\MyCart\Cart\Domain\Exceptions\PriceLowerThanZeroException;

class ProductOfferPrice
{
    /**
     * @var float
     */
    private float $value;

    /**
     * ProductOfferPrice constructor.
     * @param float $value
     * @throws PriceLowerThanZeroException
     */
    public function __construct(float $value)
    {
        if($value < 0) {
            throw new PriceLowerThanZeroException();
        }

        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getValue() :float
    {
        return $this->value;
    }
}
