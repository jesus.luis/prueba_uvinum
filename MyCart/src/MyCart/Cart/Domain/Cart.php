<?php

namespace Jesusmlg\MyCart\Cart\Domain;

use Jesusmlg\MyCart\Cart\Domain\Exceptions\CartProductLimitException;
use Jesusmlg\MyCart\Cart\Domain\Exceptions\ProductCartNotFoundException;

class Cart
{
    const MAX_PRODUCT_LINES = 10;

    private array $cartLines = [];
    private int $totalCartPrice = 0;

    public function __construct()
    {
    }

    /**
     * @param CartLine $cartLine
     * @return bool
     * @throws CartProductLimitException
     */
    public function addCartLine(CartLine $cartLine): bool
    {
        $existingProductInCartLineIndex = $this->getExistingProductInCartLineIndex($cartLine->getProduct());

        //If products exists update line with new data, else create new one line
        if ($existingProductInCartLineIndex !== null) {
            $this->cartLines[$existingProductInCartLineIndex] = $cartLine;
        } else {
            $this->checkMaxProductLines();
            $this->cartLines[] = $cartLine;
        }

        $this->totalCartPrice = $this->calculateTotalPrice();


        return true;
    }

    /**
     * @param array $cartLines
     * @return bool
     * @throws CartProductLimitException
     */
    public function addCartLineBulk(array $cartLines) :bool
    {
        /**
         * @var Cartline $cartLine
         */
        foreach ($cartLines as $cartLine) {
            $this->addCartLine($cartLine);
        }

        return true;
    }

    /**
     * @param Product $product
     * @return int|string|null
     */
    private function getExistingProductInCartLineIndex(Product $product)
    {
        /**
         * @var CartLine $cartLine
         */
        foreach ($this->cartLines as $index => $cartLine) {
            if ($cartLine->getProductId() === $product->getId()) {
                return $index;
            }
        }

        return null;
    }

    /**
     * @throws CartProductLimitException
     */
    private function checkMaxProductLines() :void
    {
        if (count($this->cartLines) >= self::MAX_PRODUCT_LINES) {
            throw new CartProductLimitException(self::MAX_PRODUCT_LINES);
        }
    }

    /**
     * @return float|int
     */
    private function calculateTotalPrice() :float
    {
        $total = 0;

        /**
         * @var CartLine $cartLine
         */
        foreach ($this->cartLines as $cartLine) {
            $total += $cartLine->getTotalPrice();
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getTotalPrice() :float
    {
        return $this->totalCartPrice;
    }


    /**
     * @param Product $product
     * @return bool
     * @throws ProductCartNotFoundException
     */
    public function deleteProductFromCart(Product $product): bool
    {
        $existingProductInCartLineIndex = $this->getExistingProductInCartLineIndex($product);

        if ($existingProductInCartLineIndex === null) {
            throw new ProductCartNotFoundException();
        }

        array_splice($this->cartLines, $existingProductInCartLineIndex, 1);

        return true;
    }

    /**
     * @return array
     */
    public function getCartLines() : array
    {
        return $this->cartLines;
    }

    /**
     * @return int
     */
    public function getMaxCartLines() :int
    {
        return self::MAX_PRODUCT_LINES;
    }
}
