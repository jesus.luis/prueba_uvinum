<?php


namespace Jesusmlg\MyCart\Cart\Domain;


use Jesusmlg\MyCart\Cart\Domain\Exceptions\BadQuantityForOfferPriceExpception;

class QuantityForOfferPrice
{
    /**
     * @var int
     */
    private int $value;

    /**
     * QuantityForOfferPrice constructor.
     * @param int $value
     * @throws BadQuantityForOfferPriceExpception
     */
    public function __construct(int $value)
    {
        if($value < 0) {
            throw new BadQuantityForOfferPriceExpception();
        }

        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
