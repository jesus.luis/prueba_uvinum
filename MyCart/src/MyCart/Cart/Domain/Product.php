<?php

namespace Jesusmlg\MyCart\Cart\Domain;

use Jesusmlg\MyCart\Cart\Domain\Exceptions\OfferPriceLowerThanNormalPriceException;

class Product
{
    /**
     * @var ProductId
     */
    private ProductId $id;
    /**
     * @var ProductName
     */
    private ProductName $productName;
    /**
     * @var ProductPrice
     */
    private ProductPrice $price;
    /**
     * @var ProductOfferPrice
     */
    private ProductOfferPrice $offerPrice;
    /**
     * @var QuantityForOfferPrice
     */
    private QuantityForOfferPrice $quantityForOfferPrice;

    /**
     * Product constructor.
     * @param ProductId $id
     * @param ProductName $productName
     * @param ProductPrice $price
     * @param ProductOfferPrice $offerPrice
     * @param QuantityForOfferPrice $quantityForOfferPrice
     * @throws OfferPriceLowerThanNormalPriceException
     */
    public function __construct(ProductId $id, ProductName $productName, ProductPrice $price, ProductOfferPrice $offerPrice, QuantityForOfferPrice $quantityForOfferPrice)
    {
        $this->checkOfferPricesIsLowerThanNormalPrice($price, $offerPrice);

        $this->id = $id;
        $this->productName = $productName;
        $this->price = $price;
        $this->offerPrice = $offerPrice;
        $this->quantityForOfferPrice = $quantityForOfferPrice;
    }

    /**
     * @param ProductPrice $productPrice
     * @param ProductOfferPrice $productOfferPrice
     * @throws OfferPriceLowerThanNormalPriceException
     */
    private function checkOfferPricesIsLowerThanNormalPrice(ProductPrice $productPrice, ProductOfferPrice $productOfferPrice): void
    {
        if ($productOfferPrice->getValue() > $productPrice->getValue()) {
            throw new OfferPriceLowerThanNormalPriceException();
        }
    }

    /**
     * @return ProductId
     */
    public function getId(): string
    {
        return $this->id->getValue();
    }

    /**
     * @return ProductName
     */
    public function getProductName(): string
    {
        return $this->productName->getValue();
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price->getValue();
    }

    /**
     * @return float
     */
    public function getOfferPrice(): float
    {
        return $this->offerPrice->getValue();
    }

    /**
     * @return int
     */
    public function getQuantityForOfferPrice(): int
    {
        return $this->quantityForOfferPrice->getValue();
    }


}
