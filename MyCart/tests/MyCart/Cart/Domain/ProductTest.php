<?php

namespace Jesusmlg\Tests\MyCart\Cart\Domain;

use Jesusmlg\MyCart\Cart\Domain\Exceptions\OfferPriceLowerThanNormalPriceException;
use Jesusmlg\MyCart\Cart\Domain\Exceptions\BadQuantityForOfferPriceExpception;
use Jesusmlg\MyCart\Cart\Domain\Exceptions\PriceLowerThanZeroException;
use Jesusmlg\MyCart\Cart\Domain\Product;
use Jesusmlg\MyCart\Cart\Domain\ProductId;
use Jesusmlg\MyCart\Cart\Domain\ProductName;
use Jesusmlg\MyCart\Cart\Domain\ProductOfferPrice;
use Jesusmlg\MyCart\Cart\Domain\ProductPrice;
use Jesusmlg\MyCart\Cart\Domain\QuantityForOfferPrice;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @throws BadQuantityForOfferPriceExpception
     * @throws OfferPriceLowerThanNormalPriceException
     * @throws PriceLowerThanZeroException
     */
    public function testProductCanBeCreated()
    {
        $cart = new Product(
            new ProductId('03d2cce9-a8ee-4ad3-a575-512934ca561d'),
            new ProductName('Jack Danields Honey'),
            new ProductPrice(18.95),
            new ProductOfferPrice(15.99),
            new QuantityForOfferPrice(3)
        );

        $this->assertInstanceOf(Product::class, $cart);
    }

    /**
     * @throws BadQuantityForOfferPriceExpception
     * @throws OfferPriceLowerThanNormalPriceException
     * @throws PriceLowerThanZeroException
     */
    public function testProductQuantityForOfferPriceException()
    {
        $this->expectException(BadQuantityForOfferPriceExpception::class);

        $cart = new Product(
            new ProductId('03d2cce9-a8ee-4ad3-a575-512934ca561d'),
            new ProductName('Jack Danields Honey'),
            new ProductPrice(18.95),
            new ProductOfferPrice(15.99),
            new QuantityForOfferPrice(-1)
        );

        $this->assertInstanceOf(Product::class, $cart);
    }

    /**
     * @throws BadQuantityForOfferPriceExpception
     * @throws OfferPriceLowerThanNormalPriceException
     * @throws PriceLowerThanZeroException
     */
    public function testOfferPriceLowerThanNormalPriceException()
    {
        $this->expectException(OfferPriceLowerThanNormalPriceException::class);

        $cart = new Product(
            new ProductId('03d2cce9-a8ee-4ad3-a575-512934ca561d'),
            new ProductName('Jack Danields Honey'),
            new ProductPrice(18.95),
            new ProductOfferPrice(19.99),
            new QuantityForOfferPrice(1)
        );

        $this->assertInstanceOf(Product::class, $cart);
    }
}
