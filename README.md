# Instrucciones de instalación

- cd MyCart
- composer install

###### Ejecución de los tests de MyCart:
- cd MyCart
- ./myCartTests.sh

###### Ejecución manual de los tests de MyCart:
- cd MyCart
- ./vendor/bin/phpunit tests

###### Ejecución matrices:
 - php matrices.php

###### Ejecución script sorting:
- php sorting.php
